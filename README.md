Abstract Factory README.md
===

![이미지](/image/abstract_image_01.PNG)

하고싶은 것이 객체를 관리하는 Factory를 생성 후 모든 관리를 Factory를 통해서 하고 싶다!

```c

int main() {
    GuiFac fac = FactoryInstance.getGuiFac();
    // 어떤 OS에서도 코드수정없이 사용가능.

    Button button = fac->creatButton();
    // Factory에서 return해줘야함!
    TextArea area = fac->createTextArea();
    // Factory에서 return해줘야함!

    button.click();
    // 등등...
}

```

```c

class FactoryInstance {
public:
    GuiFac getGuiFac() {
        switch(OS.ver) {
        case 0:
            return new MacGuiFac();
        case 1:
            return new LinuxGuiFac();
        case 2:
            return new WindowsGuiFac();
        }
    }
};

class MacGuiFac : public GuiFac {
    // 구현
};

class LinuxGuiFac : public GuiFac {
    // 구현
};

class WindowsGuiFac : public GuiFac {
    // 구현
};

```

```c

class GuiFac {
public:
    virtual Button createButton();
    virtual TextArea createTextArea();
};

```


---

[유투부1](https://www.youtube.com/watch?v=qXcxySA-Qes&list=PLGgiK7igwYTtmZo9jOLp6kIQ9W-YiznII&index=11&t=1s)<br>
[유투부2](https://www.youtube.com/watch?v=soV1C6j9kkg&list=PLGgiK7igwYTtmZo9jOLp6kIQ9W-YiznII&index=11)<br>